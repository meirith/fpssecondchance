using UnityEngine;
using System.Collections;

public class BallShoot : MonoBehaviour {
	
	public GameObject Bullet_45mm_Bullet;
	float bulletImpulse = 20f;

	
	// Update is called once per frame
	public void Shot () {
		GameObject thebullet = (GameObject)Instantiate(Bullet_45mm_Bullet, Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.rotation);
		thebullet.GetComponent<Rigidbody>().AddForce( Camera.main.transform.forward * bulletImpulse, ForceMode.Impulse);
	}
}
