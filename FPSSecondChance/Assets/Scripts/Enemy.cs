﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int EnemyHealth = 10;
    public GameObject TheZombie;
    
    void DeductPoints(int DamageAmount)
    {
        EnemyHealth -= DamageAmount;
    }
    void Update()
    {
        if (EnemyHealth <= 0)
        {
            
            TheZombie.GetComponent<Animation>().Play("Dying");
            this.GetComponent<ZombieFollow>().enabled = false;

            if (EnemyHealth <= 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
