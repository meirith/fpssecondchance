﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSInputManager : MonoBehaviour
{

    private PlayerMovement playerController;
    private MouseCursor mouseCursor;
    private float sensitivity = 3.0f;
    private LookRotation lookRotation;

    void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
        lookRotation = playerController.GetComponent<LookRotation>();
        mouseCursor = new MouseCursor();
        mouseCursor.HideCursor();
    }

    void Update()
    {
        //Player movement
        Vector2 inputAxis = Vector2.zero;
        inputAxis.x = Input.GetAxis("Horizontal");
        inputAxis.y = Input.GetAxis("Vertical");
        playerController.SetAxis(inputAxis);

        //Player jump
        if (Input.GetButton("Jump")) playerController.StartJump();

        //Mouse cursor
        if (Input.GetMouseButtonDown(0)) mouseCursor.HideCursor();
        else if (Input.GetKeyDown(KeyCode.Escape)) mouseCursor.ShowCursor();

        //Camera rotation
        Vector2 mouseAxis = Vector2.zero;
        mouseAxis.x = Input.GetAxis("Mouse X") * sensitivity;
        mouseAxis.y = Input.GetAxis("Mouse Y") * sensitivity;
        //Debug.Log("Mouse X = " + mouseAxis.x + " Y = " + mouseAxis.y);
        lookRotation.SetRotation(mouseAxis);
    }
}

