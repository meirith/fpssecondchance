﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AmmoPickup : MonoBehaviour
{
    public AudioSource AmmoSound;
    void OnTriggerEnter(Collider other)
    {
        AmmoSound.Play();
        if (Ammo.LoadedAmmo == 0)
        {
            Ammo.LoadedAmmo += 10;
            this.gameObject.SetActive(false);
        }
        else
        {
            Ammo.CurrentAmmo += 0;
            this.gameObject.SetActive(false);
        }
    }
}
